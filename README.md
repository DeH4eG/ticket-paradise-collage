# PNG images into collage
 
Make collage from provided PNG images

## Usage

If you want a fast start, just run

```shell
docker run -it --rm \
-w /app \
-v $PWD:/app registry.gitlab.com/deh4eg/ticket-paradise-collage/alpine/php-cli:7.4 \
sh -c 'composer install && ./bin/collage ds:make-collage assets output/collage.png -c 5'
```

## Result

![collage](output/collage.png)