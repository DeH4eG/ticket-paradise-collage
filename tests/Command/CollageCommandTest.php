<?php

declare(strict_types=1);

namespace Command;

use DS\Command\CollageCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CollageCommandTest extends TestCase
{
    public function testExecuteSuccess(): void
    {
        $tempCollageFile = tempnam(sys_get_temp_dir(), 'collage-');

        $this->expectOutputString(
            "Collage successfully generated into - ${tempCollageFile}"
        );

        $this->executeCommanderTester('assets', $tempCollageFile);

        unlink(tempnam(sys_get_temp_dir(), 'collage-'));
    }

    private function executeCommanderTester(string $sourcePath, string $destinationPath): void
    {
        $commandTester = new CommandTester(new CollageCommand());
        $commandTester->execute(
            [
                'sourcePath' => $sourcePath,
                'destinationPath' => $destinationPath,
            ],
            [
                'row-count' => 5
            ]
        );

        print trim($commandTester->getDisplay());
    }

    public function testExecuteNoSrouceFilesFound(): void
    {
        $this->expectException(\LengthException::class);

        $this->executeCommanderTester(
            'assetss',
            'output/collage.png'
        );
    }

    public function testExecuteDestinationFileError(): void
    {
        $this->expectException(\ImagickException::class);

        $this->executeCommanderTester(
            'assets', 'random/collage.png'
        );
    }
}
