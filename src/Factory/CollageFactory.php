<?php

declare(strict_types=1);

namespace DS\Factory;

use DS\Collage;
use DS\Contract\Collage as CollageInterface;
use DS\Contract\CollageFactory as CollageFactoryInterface;

final class CollageFactory implements CollageFactoryInterface
{
    /**
     * @param array<string> $files
     */
    public static function createCollage(
        array $files,
        string $outputFile,
        int $countInRow
    ): CollageInterface {
        return new Collage($files, $outputFile, $countInRow);
    }
}
