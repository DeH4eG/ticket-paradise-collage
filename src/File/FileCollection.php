<?php

declare(strict_types=1);

namespace DS\File;

final class FileCollection implements \IteratorAggregate, \Countable
{
    /**
     * @var array<string>
     */
    private array $files;

    /**
     * @param array<string> $files
     */
    public function __construct(array $files)
    {
        if (! $files) {
            throw new \LengthException('Provided array of files is empty');
        }

        $this->files = $files;
    }

    public function sortByName(): FileCollection
    {
        $files = $this->files;

        $callback = static function (string $a, string $b) {
            $firstFilename = (int) pathinfo($a, PATHINFO_FILENAME);
            $secondFilename = (int) pathinfo($b, PATHINFO_FILENAME);

            if ($firstFilename === $secondFilename) {
                return 0;
            }

            return $firstFilename < $secondFilename ? -1 : 1;
        };

        usort($files, $callback);

        return new self($files);
    }

    /**
     * @return $this
     */
    public function splitByRows(int $count): FileCollection
    {
        return new static(array_chunk($this->files, $count));
    }

    /**
     * @return array<string>
     */
    public function toArray(): array
    {
        return $this->files;
    }

    public function getIterator(): FileCollectionIterator
    {
        return new FileCollectionIterator($this);
    }

    /**
     * @return int|void
     */
    public function count()
    {
        return count($this->files);
    }
}
