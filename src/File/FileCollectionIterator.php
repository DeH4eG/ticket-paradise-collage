<?php

declare(strict_types=1);

namespace DS\File;

final class FileCollectionIterator implements \Iterator
{
    private int $position = 0;

    /**
     * @var array<string>
     */
    private array $fileCollection;

    public function __construct(FileCollection $fileCollection)
    {
        $this->fileCollection = $fileCollection->toArray();
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->fileCollection[$this->position];
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->fileCollection[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }
}
