<?php

declare(strict_types=1);

namespace DS\Command;

use DS\Factory\CollageFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class CollageCommand extends Command
{
    protected static $defaultName = 'ds:make-collage';

    protected function configure(): void
    {
        $this
            ->setHelp('Generate collage by provided row count')
            ->addArgument(
                'sourcePath',
                InputArgument::REQUIRED,
                'Directory path where images lie. (Currently supports only PNG images)'
            )->addArgument(
                'destinationPath',
                InputArgument::REQUIRED,
                'Destination file path'
            )->addOption(
                'row-count',
                '-c',
                InputOption::VALUE_OPTIONAL,
                'Count of rows. By default - 5',
                5
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $sourcePath = $input->getArgument('sourcePath');
        $outputFile = $input->getArgument('destinationPath');
        $countInRow = (int) $input->getOption('row-count');

        $files = glob($sourcePath . '/*.png');
        $collage = CollageFactory::createCollage(
            $files,
            $outputFile,
            $countInRow
        );

        if ($collage->run()) {
            $output->writeln(
                '<info>' .
                "Collage successfully generated into - ${outputFile}" .
                '</info>'
            );
            return Command::SUCCESS;
        }

        return Command::FAILURE;
    }
}
