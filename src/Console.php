<?php

declare(strict_types=1);

namespace DS;

use Symfony\Component\Console\Application;

final class Console
{
    private const COMMANDS = [
        \DS\Command\CollageCommand::class,
    ];

    private Application $symfonyApplication;

    public function __construct()
    {
        $this->symfonyApplication = new Application();
    }

    public static function make(): void
    {
        (new self())->run();
    }

    /**
     * @throws \Exception
     */
    public function run(): void
    {
        $this->registerCommands();
        $this->symfonyApplication->run();
    }

    private function registerCommands(): void
    {
        foreach (self::COMMANDS as $command) {
            $this->symfonyApplication->add(new $command());
        }
    }
}
