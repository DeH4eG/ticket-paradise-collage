<?php

declare(strict_types=1);

namespace DS\Contract;

interface Collage
{
    public function run(): bool;
}
