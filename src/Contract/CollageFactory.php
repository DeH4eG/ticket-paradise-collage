<?php

declare(strict_types=1);

namespace DS\Contract;

interface CollageFactory
{
    /**
     * @param array<string> $files
     */
    public static function createCollage(
        array $files,
        string $outputFile,
        int $countInRow
    ): Collage;
}
