<?php

declare(strict_types=1);

namespace DS;

use DS\Contract\Collage as CollageInterface;
use DS\File\FileCollection;
use Imagick;
use ImagickException;

final class Collage implements CollageInterface
{
    private const TRANSPARENT_COLOR = 'transparent';

    private const IMAGE_FORMAT_PNG = 'png';

    private FileCollection $files;

    private int $countInRow;

    private string $outputFile;

    /**
     * @param array<string> $files
     */
    public function __construct(
        array $files,
        string $outputFile,
        int $countInRow
    ) {
        if ($countInRow <= 0) {
            throw new \ValueError(
                'Please provide positive count in row value.' .
                " Current value: ${countInRow}"
            );
        }

        $this->files = new FileCollection($files);
        $this->countInRow = $countInRow;
        $this->outputFile = $outputFile;
    }

    /**
     * @throws ImagickException
     */
    public function run(): bool
    {
        $collage = $this->draw();
        $collage->setImageFormat(self::IMAGE_FORMAT_PNG);

        return $collage->writeImage($this->outputFile);
    }

    /**
     * @throws ImagickException
     */
    private function draw(): Imagick
    {
        $canvas = new Imagick();

        $rows = $this->files->sortByName()->splitByRows($this->countInRow);

        foreach ($rows as $rowKey => $row) {
            $rowImage = new Imagick();
            $rowImage->setBackgroundColor(self::TRANSPARENT_COLOR);

            foreach ($row as $fileKey => $file) {
                $image = new Imagick($file);
                $image->setImageBackgroundColor(self::TRANSPARENT_COLOR);

                if ($fileKey !== array_key_last($row)) {
                    $image->extentImage(
                        $image->getImageWidth() + 10,
                        $image->getImageHeight(),
                        0,
                        0
                    );
                }

                if ($rowKey !== array_key_last($rows->toArray())) {
                    $image->extentImage(
                        $image->getImageWidth(),
                        $image->getImageHeight() + 10,
                        0,
                        0
                    );
                }

                $rowImage->addImage($image);
            }

            $rowImage->resetIterator();
            $combined = $rowImage->appendImages(false);
            $canvas->addImage($combined);
        }

        $canvas->resetIterator();

        return $canvas->appendImages(true);
    }
}
