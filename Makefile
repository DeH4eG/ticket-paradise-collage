run_tests: test insights

test:
	./vendor/bin/phpunit --color=always tests

insights:
	./vendor/bin/phpinsights --no-interaction --min-quality=95 --min-complexity=85 --min-architecture=90 --min-style=95